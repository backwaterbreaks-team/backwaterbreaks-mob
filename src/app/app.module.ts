import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { FormsModule } from '@angular/forms';
import { HttpModule, BrowserXhr } from '@angular/http';
import { MyApp } from './app.component';
import { LoginPage } from "../pages/login/login";

import { TabsPage } from "../pages/tabs/tabs";


import { NgCalendarModule  } from 'ionic2-calendar';
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { MonthViewComponent } from 'ionic2-calendar/monthview';
import { WeekViewComponent } from 'ionic2-calendar/weekview';
import { DayViewComponent } from 'ionic2-calendar/dayview';


import { SpinnerModule } from 'angular2-spinner';
import { CacheModule } from "ionic-cache/dist";

import { RestServiceProvider } from '../providers/rest-service/rest-service';

import { LogoutProvider } from '../providers/logout/logout';
import { AddAvailabilityPage } from "../pages/addAvailability/addAvailability";
import { AccountService } from "../models/account.interface";

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    AddAvailabilityPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    SpinnerModule,
    HttpModule,
      NgCalendarModule,
    FormsModule,
    CacheModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp 
  ],
  entryComponents: [
    MyApp,
    LoginPage,
    AddAvailabilityPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AccountService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestServiceProvider,
    LogoutProvider
  ]
})
export class AppModule {}
