import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvailabilityPage } from './availability';
import { NgCalendarModule } from "ionic2-calendar";
import { SpinnerModule } from 'angular2-spinner';
@NgModule({
  declarations: [
    AvailabilityPage,
  ],
  imports: [
    SpinnerModule,
    NgCalendarModule,
    IonicPageModule.forChild(AvailabilityPage),
  ],
})
export class AvailabilityPageModule { }
