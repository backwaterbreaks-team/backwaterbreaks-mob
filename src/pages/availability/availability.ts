import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams, MenuController, AlertController, ToastController } from 'ionic-angular';
import { AccountService } from '../../models/account.interface';
import { NgCalendarModule } from 'ionic2-calendar';
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { MonthViewComponent } from 'ionic2-calendar/monthview';
import { WeekViewComponent } from 'ionic2-calendar/weekview';
import { DayViewComponent } from 'ionic2-calendar/dayview';

import { LoginPage } from "../login/login";

import { RestServiceProvider } from "../../providers/rest-service/rest-service";
import { LogoutProvider } from "../../providers/logout/logout";
import { AddAvailabilityPage } from "../addAvailability/addAvailability";


@IonicPage()
@Component({
    selector: 'page-availability',
    templateUrl: 'availability.html',
})
export class AvailabilityPage {

    
    private userAcct = {};
    private listings = {};
    private selectedId: number;
    private business = [{}];
    private selectedvalue: string;
    private listing_type;
    private datesAvailable: any = [{}];
    private loading: boolean = false;
    private spinner: boolean = false;
    private imgUrl: string;
    private FindImageUrl;
    private months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    private newViewTitleString: string;
    private eventSource;
    private viewTitle;
    private isToday: boolean;
    private addRemoveTitleText: any = '';
    private calendarMonth;
    private j = 1;
    private displayCalendar: boolean = false;
    private eventTitle = [];
    private DateString: string;
    private dateStringArr: string[];


    constructor(private logoutService: LogoutProvider, private app: App, private toast: ToastController, private menuCtrl: MenuController, private navCtrl: NavController,
        private alertCtrl: AlertController,
        private restService: RestServiceProvider, private navParams: NavParams) {
        this.userAcct = this.navParams.get('bookings');
    }

    openMenu() {
        this.menuCtrl.open();
    }


    ionViewDidLoad() {
        setTimeout(() => {
            this.spinner = true;
        }, 3000);
        let bookings = {};
        let userType: string;
        let userId;
        this.restService.getRequest('action/retrieveUser.action', null).
            subscribe((result: any) => {
                bookings = result.user;
                userType = result.user.userType;
                userId = result.user.id;


                if (userType == 'ADMIN') {
                    this.getAllBusiness();
                }

                if (userType == 'VENDOR') {
                    this.getBusinessesForOperator();
                }
            });
    }
    //function to get All Business details
    getBusinessesForOperator() {
        this.restService.getRequest('action/GetBusinessUnitsForOperator.action', null).
            subscribe((result: any) => {
                this.business = result.businessUnits;
            }
            ,
            err => {
                this.toast.create({
                    message: 'Connection Problem,Unable to get Businesses',
                    duration: 3000
                }).present();
            });


    }


    getAllBusiness() {
        this.restService.getRequest('action/getAllBusinessUnits.action', null).
            subscribe((result: any) => {
                this.business = result.businessUnits;
            }
            ,
            err => {
                this.toast.create({
                    message: 'Connection Problem,Unable to get Businesses',
                    duration: 3000
                }).present();
            });
    }

    AddRemoveAvailability() {
        this.navCtrl.push(AddAvailabilityPage, {
            businessName: this.business
        });
    }

    /*
    onListingChange() gets the imageUrl based upon the businessId and listingId
    */
    onListingChange(selectedImageId) {
        /*
          cannot hit update button if the 
          business,listing,start and enddate 
          are undefined i.e not selected
          */
        if (this.selectedId == undefined && this.selectedvalue == undefined) {
            this.loading = false;
        }
        else {
            this.loading = true;
        }
        this.FindImageUrl = selectedImageId;
        //displays the imageUrl for the selected businessID and listingID
        for (var key in this.listings) {
            if (Object.prototype.hasOwnProperty.call(this.listings, key)) {
                if (this.FindImageUrl == this.listings[key].id) {
                    this.imgUrl = this.listings[key].imageUrl;
                }

            }

        }

    }

    /*
    Displays all the listings for the selectedBusinessId
    */
    getListing(selectedId) {

        var jsonData = {
            "business_unit_id": selectedId
        }
       
        this.restService.getRequest('action/getListingsForBusinessUnit.action', jsonData).
            subscribe((result: any) => {
                this.listings = result.listings;
            }
            ,
            err => {
                this.toast.create({
                    message: 'Connection Problem Unable to get Listings',
                    duration: 3000
                }).present();
            });

    }

    /* GetType() keeps a track of changing 
     businessname on dropdown selection
     to obtain listings for correct businessId.d.
     Everytime the dropdown selection changes, the listing_type gets updated.
     */
    getType(event) {
        this.listing_type = "" + event;
        this.getListing(this.listing_type);
    }


    /*
   To obtain values from object on passing of the keys.
   To display values in *ngFor.
   For array iteration.
     */
    generateArray(obj) {
        return Object.keys(obj).map((key) => { return { key: key, value: obj[key] } });
    }

    calendar = {
        mode: 'month',
        currentDate: new Date()
    }; // these are the variable used by the calendar.
    onViewTitleChanged(title) {
        this.displayCalendar = false;
        this.viewTitle = title;
    }

    onEventSelected(event) {
       
    }

    changeMode(mode) {
        this.calendar.mode = mode;
    }


    /*
    checking whether the title property of events array has either Add/Remove;
    accordingly update the arg passed
    */

    addremoveTitle(arg: any) {
        
        if (arg == "add") {
         
            return 'remove';
        }
        else {
          
            return 'add';
        }
    }

    /*
Displays an alertCtrl when user clicks on a particular date
so that user can Add/Remove Availabiity
    */


    onTimeSelected(ev) {


        var d = new Date(ev.selectedTime);
        var year = d.getFullYear();
        var date = d.getDate();
        var month = d.getMonth() + 1;
        let alert = this.alertCtrl.create();

        this.addRemoveTitleText = this.addremoveTitle(this.eventTitle[date - 1]);

        alert.setTitle('Do you want to ' + this.addremoveTitle(this.eventTitle[date - 1]) + ' availability on ' + date + '-' + month + '-' + year);
        alert.addInput({
            type: 'checkbox',
            label: 'Day',
            name: 'day',
            value: 'DAY',
            checked: true
        });

        alert.addInput({
            type: 'checkbox',
            label: 'Overnight',
            value: 'OVERNIGHT',
            checked: true
        });

        alert.addButton('Cancel');
        alert.addButton({
            text: 'Ok',
            handler: data => {
                var url;

                if (this.addRemoveTitleText == "add") {
                    url = "action/addAvailabilities2.action";
                }
                else {
                    url = "action/removeAvailabilities2.action";
                }
                var r = /\d+/;
                var s = this.selectedvalue;
                var listing = s.match(r);//splitting part after _ to obtain only the id
                var finalData = [];
      
                for (var q = 0; q < data.length; q++) { //append DAY,OVERNIGHT at the end of lisitng id
                    if (data[q] == 'DAY') {
                        finalData[q] = listing + "_DAY";
                     
                    }
                    else if (data[q] == 'OVERNIGHT') {
                        finalData[q] = listing + "_OVERNIGHT";
                      
                    }
                }
              
               var newmonth; /*checks if current month is greater or less than 10(Oct)
                                    inorder to add 0 before the month if it is less than 10
                                */
                if(month>9){
                  
                    newmonth = month;
                }
                else{
                    newmonth = "0" + month;
                }
                var jsonAvailabilityData = {

                    "listing_ids": JSON.stringify(finalData),
                    "start_date": ((date) + "-" +  (newmonth) + "-" + year).replace(/['"]+/g, ''),
                    "end_date": ((date) + "-" +  (newmonth) + "-" + year).replace(/['"]+/g, '')
                }


                this.restService.postRequest(url, jsonAvailabilityData)
                    .subscribe(response => {
                     
                    });
            }
        });
        alert.present();

    }



    onCurrentDateChanged(event: Date) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        event.setHours(0, 0, 0, 0);
        this.isToday = today.getTime() === event.getTime();
    }

    createRandomEvents(val) {
        var events = [];
        var eventDates = [];
        var datesArr = new Array(); //store dates of availability
        var availArr = new Array();

        for (var key in val) {
            if (Object.prototype.hasOwnProperty.call(val, key)) {

                var availValue = val[key]; //has true/false value

                var count = 0; //count number of values in array datesArr
                count++;
                for (var i = 0; i < count; i++) {
                    datesArr[i] = key;
                    availArr[i] = availValue;
                    this.DateString = datesArr[i];
                    this.dateStringArr = this.DateString.split("-");
                    var ddate = parseInt(this.dateStringArr[0]);
                    var dmonth = parseFloat(this.dateStringArr[1]);
                    var dyear = parseInt(this.dateStringArr[2]);

                    var date = new Date(dyear, dmonth - 1, ddate);
                    var eventType = availArr[i];
                    var startTime;
                    var endTime;
                    if (eventType === true) {
                        startTime = new Date(Date.UTC(dyear, dmonth - 1, ddate));
                        endTime = new Date(Date.UTC(dyear, dmonth - 1, ddate));
                        events.push({
                            title: 'add',
                            startTime: startTime,
                            endTime: endTime,
                            eventColor: 'green'
                           
                        });

                    }
                    else {
                        startTime = new Date(Date.UTC(dyear, dmonth - 1, ddate));
                        endTime = new Date(Date.UTC(dyear, dmonth - 1, ddate));

                        events.push({
                            title: 'remove ',
                            startTime: startTime,
                            endTime: endTime,
                            eventColor: 'red'
                            
                        });
                        eventDates = events.map(obj => obj.startTime + obj.endTime);
                    }
                }

            }
        }
        this.eventTitle = events.map(obj => obj.title);

    
        return events;

    }
    markDisabled = (date: Date) => {
        var current = new Date();
        current.setHours(0, 0, 0);
        return date < current;
    };


    ngOnInit() {
        this.displayCalendar = false;
    }

    /*
    load calendar events and map it to calendar
    */
    LoadEvents(value) {


        this.displayCalendar = true;
      
        this.newViewTitleString = this.viewTitle;
        for (var j = 0; j < this.months.length; j++) {
            if (this.newViewTitleString.includes(this.months[j])) {
                this.calendarMonth = j;
            }
            else {
                continue;
            }
        }

        //to calculate firstday and last day of every month
        let date = new Date(), year = date.getFullYear(), month = this.calendarMonth;
        let firstDay = new Date(year, month, 1);
        let lastDay = new Date(year, month + 1, 0);
        let fdate = firstDay.getDate();
        let fmonth = (firstDay.getMonth() + 1);
        let fyear = firstDay.getFullYear();
        let ldate = lastDay.getDate();
        let lmonth = (lastDay.getMonth() + 1);
        let lyear = lastDay.getFullYear();

        var jsonData = {
            "listing_id": value,
            "start_date": (fdate + '-' + fmonth + '-' + fyear).replace(/['"]+/g, ''),
            "end_date": (ldate + '-' + lmonth + '-' + lyear).replace(/['"]+/g, '')
        }


       
        this.restService.getRequest('action/getAvailabilitiesForListing.action', jsonData).
            subscribe((result: any) => {

                this.datesAvailable.push(result.dailyAvailabilities.availabilities);
                var secondKey = Object.keys(this.datesAvailable)[this.j]; //fetched the key at second index

                this.j = ++this.j;
                for (var key in this.datesAvailable) {
                    if (Object.prototype.hasOwnProperty.call(this.datesAvailable, key)) {
                        var val = this.datesAvailable[secondKey];

                        this.eventSource = this.createRandomEvents(val);

                        break;

                    }
                }

            }
            ,
            err => {
                this.toast.create({
                    message: 'Connection Problem Unable to get Availabilities',
                    duration: 3000
                }).present();
            }

            );



    }




    logout() {

        this.logoutService.Logout();
    }



}
