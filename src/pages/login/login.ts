import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx'
import { TabsPage } from "../tabs/tabs";
import { RestServiceProvider } from "../../providers/rest-service/rest-service";
import { AccountService } from "../../models/account.interface";

@IonicPage()
@Component({
  selector: 'page-login',

  templateUrl: 'login.html',
})
export class LoginPage {
private  acct = {};
private email;
private password;
  private slideOneForm: FormGroup;


  constructor(private Acc: AccountService ,private alertCtrl: AlertController, 
    private navCtrl: NavController, private toast: ToastController, 
    private formBuilder: FormBuilder, private navParams: NavParams,
     private http: Http,private rest:RestServiceProvider) {

    this.slideOneForm = formBuilder.group({
      email: ['', Validators.compose([Validators.maxLength(50), Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]{5,20}\.+[a-z]{2,7}'), Validators.required])],
      password: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('(?=.*\d)(?=.*[a-z])(?=.*).{5,}'), Validators.required])]
    });


  }


  gottoHome() {
    this.Acc.setemail = this.email;
    this.Acc.setpassword = this.password;

    let submitAttempt: boolean = false;
    if (this.Acc.getemail == undefined || this.Acc.getpassword == undefined) {
      submitAttempt = false;
      let alertdisplay = this.alertCtrl.create({
        title: 'Sorry',
        subTitle: 'You must fill in both details',
        buttons: ['OK']
      });
      alertdisplay.present();

    }
    else {
      submitAttempt = true;
    }

    if (submitAttempt == true) {
      
      var jsonData= {

        "email_login" : this.Acc.getemail,
        "password_login": this.Acc.getpassword
      }
      this.rest.postRequest('action/userLogin.action',jsonData)
        .subscribe((res: any) => {
          var property:string;
          var key:string;
        
          for (var key in res) {
            var arr1 = res[key];
            for( var i = 0; i < arr1.length; i++ ) {
                var obj = arr1[ i ];
                for (var prop in obj) {
                    if(obj.hasOwnProperty(prop)){
                        
                        property = obj["errorMessage"];
                       key= obj["key"];
                    }
                }
            }
         }
       
         if(key=='VALIDATION_ERROR'){
          this.toast.create({
                message: property,
                duration: 3000,
                position: 'middle'
              }).present();
         }
         else  if(key=='PASSWORD_NOT_MATCHING'){
          this.toast.create({
                message: property,
                duration: 3000,
                position: 'middle'
              }).present();
         }
        
         else  if(key=='USER_NOT_EXISTS'){
          this.toast.create({
                message: property,
                duration: 3000,
                position: 'middle'
              }).present();
         }
         else if(key == 'user'){
          
           this.toast.create({
                message: 'Logged in',
                duration: 3000,
                position: 'top'
              }).present();
              this.navCtrl.push(TabsPage, {
                bookings: this.Acc
              });
         }
      
        },
        (error: any) => {
          this.toast.create({
            message: 'Server Error',
            duration: 3000,
            position: 'top'
          }).present();
        });


    }


  }


}
