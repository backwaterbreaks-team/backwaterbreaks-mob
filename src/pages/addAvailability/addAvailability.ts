import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { RestServiceProvider } from "../../providers/rest-service/rest-service";


@IonicPage()
@Component({
  selector: 'page-addAvailability',
  templateUrl: 'addAvailability.html',
})
export class AddAvailabilityPage {

  //Variable Declarations

  private listings = {};
  private selectedId: number;
  private business = [{}];
  private selectedvalue: string;
  private loading: boolean = false;

  private day = true; //takes checkbox day value
  private overnight = true;//takes checkbox overnight value value
  private action: any = 'add'; //takes value for radio values

  private currentDate = new Date().toJSON().split('T')[0];//dateObject (min check)
  private startdate = new Date();//displays startDate(today's Date) in placeholder
  //dateObject(min check)
  private nextcurrentDate = new Date(this.startdate.getFullYear(), this.startdate.getMonth(), this.startdate.getDate() + 1).toJSON().split('T')[0];
  private enddate = new Date(this.startdate.getFullYear(), this.startdate.getMonth(), this.startdate.getDate() + 1);//display in placeholder
  // s = new Date(this.startdate.getFullYear(), this.startdate.getMonth(), this.startdate.getDate());
  // e = new Date(this.enddate.getFullYear(), this.enddate.getMonth(), this.enddate.getDate());
  private listing_type;
  private datesAvailable: any = [{}];
  private datesavailability: any = [{}];


  private listingid1;
  private listingid2;
  private AllListingIds = new Array();
  private UpdateListings: any = {};
  private updateurl;
  private check_if_action_selected: boolean = false;

  private selectedIndex: number = 0;
  private imgUrl: string;
  private FindImageUrl;

  private dates: string;
  private datess: string;



  constructor(private toast: ToastController,
    private alertCtrl: AlertController,
    private navCtrl: NavController, private navParams: NavParams,
    private restService: RestServiceProvider) {
    //Get the Business name from Availability Page passed in navParam
    this.business = this.navParams.get('businessName');

  }


  /* GetListing() takes selectedId as param as gets the Listing for selectedId 
  and displays the results in listings = {}.
  the response from API is stored in result.
  listings is the name of JSON data attached to result.listings.
  Incase of errors, they are displayed using toast.
  */
  getListings(selectedId) {


    var jsonData = {
      "business_unit_id": selectedId
    }
    this.restService.getRequest('action/getListingsForBusinessUnit.action', jsonData).
      subscribe((result: any) => {
        this.listings = result.listings;

      }
      ,
      err => {
        this.toast.create({
          message: 'Unable to Display Listings',
          duration: 3000
        }).present();
      }
      );

  }

  /*
To obtain values from object on passing of the keys.
To display values in *ngFor.
For array iteration.
  */
  generateArray(obj) {
    return Object.keys(obj).map((key) => { return { key: key, value: obj[key] } });
  }

  /*
  onListingChange() gets the imageUrl based upon the businessId and listingId
  */
  onListingChange(selectedImageUrl) {
    /*
    cannot hit update button if the 
    business,listing,start and enddate 
    are undefined i.e not selected
    */
    if (this.selectedId == undefined && this.selectedvalue == undefined && this.startdate == undefined && this.enddate == undefined) {
      this.loading = false;

    }
    else {
      this.loading = true;

    }
   
    this.FindImageUrl = selectedImageUrl;

    //displays the imageUrl for the selected businessID and listingID

    for (var key in this.listings) {

      if (Object.prototype.hasOwnProperty.call(this.listings, key)) {

        if (this.FindImageUrl == this.listings[key].id) {

          this.imgUrl = this.listings[key].imageUrl;
        }

      }

    }
  }

  /* GetType() keeps a track of changing 
  businessname on dropdown selection
  to obtain listings for correct businessId.d.
  Everytime the dropdown selection changes, the listing_type gets updated.
  */
  getType(event) {
    this.listing_type = "" + event;

    this.getListings(this.listing_type);
  }

  /* 
  When cancel button is clicked, 
  go to the previous page by popping the navigation stack
  */
  Cancel() {
    this.navCtrl.pop();
  }




  /*
  Add availability function
  */
  Update() {
    /*
      selectedValue contains the Listing_id along withn_DAY or _OVERNIGHT.
      s.match(r) splits the id from _DAY/_OVERNIGHT
    */
   
    var r = /\d+/;
    var s = this.selectedvalue;
    var listing = s.match(r);


    /*
    IF-ELSE functions check if  both _DAY and _OVERNIGHT or either of them are selected.
    If none of them are selected than
     an alert is displayed to the user 
     to select atleast one of the options.
    */
    if (this.day == true && this.overnight == true) {

      this.listingid1 = listing + "_DAY";
      this.listingid2 = listing + "_OVERNIGHT";
      this.AllListingIds.push(
        new Array(this.listingid1, this.listingid2)
      )
      this.check_if_action_selected = true;
    }

    else if (this.day == true) {
      this.listingid1 = listing + "_DAY";
      this.AllListingIds.push(
        new Array(this.listingid1)
      )
      this.check_if_action_selected = true;
    }
    else if (this.overnight == true) {
      this.listingid2 = listing + "_OVERNIGHT";
      this.AllListingIds.push(
        new Array(this.listingid2)
      )
      this.check_if_action_selected = true;
    }
    else {

      let alertdisplay = this.alertCtrl.create({
        title: 'Sorry',
        subTitle: 'You must select atleast one action',
        buttons: ['OK']
      });
      alertdisplay.present();
      this.check_if_action_selected = false;
    }

    /*
    checks if either add or remove is selected. 
    by default it is add.
    */
    if (this.check_if_action_selected == true) {

      if (this.action == 'add') {
        this.updateurl = "action/addAvailabilities2.action";

      }
      else {
        this.updateurl = "action/removeAvailabilities2.action";

      }


      /*
      split('-') is done to pass the date in a particular format
      */
      var arr = this.dates.split("-");//startdate entered by user
      var arr1 = this.datess.split("-");//enddate entered by user

      var jsonAvailabilityData = {

        "listing_ids": JSON.stringify([this.listingid1, this.listingid2]),
        "start_date": (arr[2] + "-" + arr[1] + "-" + arr[0]).replace(/['"]+/g, ''),
        "end_date": (arr1[2] + "-" + arr1[1] + "-" + arr1[0]).replace(/['"]+/g, '')
      }

      this.restService.postRequest(this.updateurl, jsonAvailabilityData)
        .subscribe(response => {

        }
        ,
        err => {
          this.toast.create({
            message: 'Unable to Update Availability',
            duration: 3000
          }).present();
        });
      this.navCtrl.pop();
    }





  }

}
