import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddAvailabilityPage } from "./addAvailability";


@NgModule({
  declarations: [
    AddAvailabilityPage,
  ],
  imports: [
    IonicPageModule.forChild(AddAvailabilityPage),
  ],
})
export class AddremovePageModule { }
