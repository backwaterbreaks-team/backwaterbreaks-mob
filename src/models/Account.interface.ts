
import {Injectable} from '@angular/core';

@Injectable()
export class AccountService {

  private email:string;
  private password:string;

  set setemail(value:string) {
    this.email = value
  }

  get getemail():string {
    return this.email;
  }

  set setpassword(value:string) {
    this.password = value
  }

  get getpassword():string {
    return this.password;
  }
}