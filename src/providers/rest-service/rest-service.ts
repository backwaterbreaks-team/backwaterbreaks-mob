import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from "rxjs/Observable";
import { CacheService } from "ionic-cache/dist";

@Injectable()
export class RestServiceProvider {
  rootUrl = "http://localhost:8888/";
  constructor(private http: Http, private cache: CacheService) { }

  getRequest(url, JsonData) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let options = new RequestOptions({ headers: headers, withCredentials: true });
/*
Some GET functions don't pass any Json Data 
hence null is passed in that case
If null is passed, use only the rootUrl 
and the url passed to obtain the response
*/
    if (JsonData == null) {
      return this.http.get(this.rootUrl + url, options)
        .map(response => {
          return response.json();
        });
    }
    /*
    If JsonData is passed the length of it is going to
     be more than or equal to 1.
     Iterate in JsonData and add it to string inputs.
     Remove the last '&' symbol and pass 
     the data along with rootUrl and url
    */
    else if (Object.keys(JsonData).length >= 1) {
      let inputs = "";
      for (var key in JsonData) {
        if (JsonData.hasOwnProperty(key)) {
          var val = JsonData[key];
          inputs = inputs + key + "=" + (val) + "&";
        }
      }

      inputs = inputs.substring(0, inputs.length - 1);
      return this.http.get(this.rootUrl + url + "?" + inputs, options)
        .map(response => {
          return response.json();
        }).catch(this.handleError);
    }

  }

  public handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Problems with Server. Please try again later';
      return Observable.throw(errMsg);
  }





  postRequest(url, jsonData) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let options = new RequestOptions({ headers: headers, withCredentials: true });
/*
jsonData is passed and keys 
and values are added to string named inputs 
*/
    let inputs = "";
    for (var key in jsonData) {
      if (jsonData.hasOwnProperty(key)) {
        var val = jsonData[key];
        inputs = inputs + key + "=" + (val) + "&";
      }
    }

    /*
    the end of the string will have an additional '&' sign
    hence to delete the last symbol substring function is used.
    */
    inputs = inputs.substring(0, inputs.length - 1);
    return this.http.post(this.rootUrl + url, inputs, options)
      .map(
      (data: Response) => {
        return data.json();
      });
  }


}