
import { Injectable } from '@angular/core';
import { AlertController, App } from "ionic-angular";
import { LoginPage } from "../../pages/login/login";



@Injectable()
export class LogoutProvider {

    constructor(private app: App,private alertCtrl: AlertController) { }

    Logout() {
        
        let confirm = this.alertCtrl.create({
            title: 'Are you sure you want to logout?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        
                    }
                },
                {
                    text: 'Okay',
                    handler: () => {

                        this.app.getRootNav().setRoot(LoginPage);
                    }
                }]
        });
        confirm.present();



    }






}